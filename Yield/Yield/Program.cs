﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yield
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = CreatRandom(30).ToList();

            List<int> res = list.GretThen(50).EvenNumber().GetMax().ToList();

            foreach (var item in res)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<int> CreatRandom(int count)
        {
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                yield return rnd.Next(0, 100);
            }

        }


    }

    static class MyClass
    {
        public static IEnumerable<int> EvenNumber(this IEnumerable<int> source)
        {
            foreach (var item in source)
            {
                if (item % 2 == 0)
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<int> GretThen(this IEnumerable<int> source, int count)
        {
            foreach (var item in source)
            {
                if (item > count)
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<int> GetMax(this IEnumerable<int> source)
        {
            int max = 0;
            foreach (var item in source)
            {
                if (item > max)
                {
                    max = item;
                    yield return max;
                }
            }
        }
    }
}
